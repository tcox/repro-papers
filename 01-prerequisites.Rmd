# Prerequisites {#prereq}

Participants should install required software and register on the demonstrated collaboration platform before the course.
A basic knowledge of R is strongly recommended to take away all lessons from the course.

## Install git

> <img style="float: right;" width="120" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/512px-Git-logo.svg.png" /> _Git (/ɡɪt/) is a version control system for tracking changes in computer files and coordinating work on those files among multiple people. It is primarily used for source code management in software development, but it can be used to keep track of changes in any set of files._ [[Wikipedia](https://en.wikipedia.org/wiki/Git)]

**Install the latest version of git from [https://git-scm.com/downloads](https://git-scm.com/downloads)** (or use your operating system's package manager, homebrew, etc. - see [Installing git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)).

## Install R & RStudio

> <img style="float: right;" width="120" src="https://www.r-project.org/logo/Rlogo.png" /> _R is a programming language and free software environment for statistical computing and graphics that is supported by the R Foundation for Statistical Computing. The R language is widely used among statisticians and data miners for developing statistical software and data analysis._ [[Wikipedia](https://en.wikipedia.org/wiki/R_(programming_language))]

**Install the latest version of R from [https://cloud.r-project.org/](https://cloud.r-project.org/)**.

> <img style="float: right;" width="120" src="https://upload.wikimedia.org/wikipedia/en/f/f0/RStudio_logo.png" /> _RStudio is a free and open-source integrated development environment (IDE) for R, a programming language for statistical computing and graphics._ [[Wikipedia](https://en.wikipedia.org/wiki/RStudio)]

**Install the latest version of RStudio Desktop (Open Source License) from [https://www.rstudio.com/products/rstudio/download/](https://www.rstudio.com/products/rstudio/download/)**.

## GitLab

> <img style="float: right;" width="120" src="https://upload.wikimedia.org/wikipedia/commons/c/c6/GitLab_logo.png" /> _GitLab is a web-based Git repository manager with wiki and issue tracking features, using an open source license, developed by GitLab Inc._ [[Wikipedia](https://en.wikipedia.org/wiki/GitLab)] 

**Create an account at [https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in)**.

--- 
title: "Writing reprocible geoscience papers using R Markdown, Docker, and GitLab"
author: "Daniel Nüst, Vicky Steeves, Rémi Rampin, Markus Konkol, Edzer Pebesma"
description: "Material for a short course at the EGU Generaly Assembly 2018."
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
output:
  bookdown::gitbook:
    css: [style.css, toc.css]
    config:
      toc:
        before: |
          <li><a href="https://www.egu2018.eu/">EGU General Assembly 2018</a></li>
          <li><a href="http://meetingorganizer.copernicus.org/EGU2018/session/28650">SC1.13</a></li>
        after: |
          <li><a href="https://gitlab.com/RLesur/bookdown-gitlab-pages" target="blank">Published with bookdown &amp; GitLab</a></li>
          <li><a href="http://o2r.info" target="blank"><img src="http://o2r.info/public/images/logo-transparent.png" alt="o2r logo" height="42" /></a></li>
          <li><a href="https://datascience.nyu.edu/" target="blank"><img src="https://cds.nyu.edu/wp-content/themes/datascience/images/nyu_logo.png" alt="NYU CDS logo" height="42" /></a></li>
      edit: https://gitlab.com/VickySteeves/repro-papers/edit/master/%s
      download: ["pdf", "epub", "mobi"]
      sharing:
        facebook: no
        twitter: yes
        google: no
        linkedin: no
        weibo: no
        instapper: no
        vk: no
  bookdown::pdf_book:
    latex_engine: xelatex
    citation_package: natbib
  bookdown::epub_book: default
---

# Introduction

[<img alt="EGU General Assembly Logo (C) 2018 EGU" width="300" src="https://static.egu.eu/static/14355/logos/egu/egu_ga/egu_ga.svg" />](https://www.egu2018.eu/)

[Short course session description from [http://meetingorganizer.copernicus.org/EGU2018/session/28650](http://meetingorganizer.copernicus.org/EGU2018/session/28650)]

Reproducibility is unquestionably at the heart of science. Scientists face numerous challenges in this context, not least the lack of concepts, tools, and workflows for reproducible research in Today's curricula.
This short course introduces established and powerful tools that enable reproducibility of computational geoscientific research, statistical analyses, and visualisation of results using R (http://www.r-project.org/) in two lessons:

**1. Reproducible Research with R Markdown**
Open Data, Open Source, Open Reviews and Open Science are important aspects of science today.
In the first lesson, basic motivations and concepts for reproducible research touching on these topics are briefly introduced.
During a hands-on session the course participants write R Markdown (http://rmarkdown.rstudio.com/) documents, which include text and code and can be compiled to static documents (e.g. HTML, PDF).
R Markdown is equally well suited for day-to-day digital notebooks as it is for scientific publications when using publisher templates.

**2. GitLab and Docker**
In the second lesson, the R Markdown files are published and enriched on an online collaboration platform.
Participants learn how to save and version documents using GitLab (http://gitlab.com/) and compile them using Docker containers (https://docker.com/).
These containers capture the full computational environment and can be transported, executed, examined, shared and archived. 
Furthermore, GitLab's collaboration features are explored as an environment for Open Science.

**All material available at [https://osf.io/qd9nf/](https://osf.io/qd9nf/).**
